# Deploy OpenStack on docker with Kolla-Ansible
The cluster we have
- [ ] admin: gitlab-runner, kolla-ansible, monitoring
- [ ] controller[01:03]: control, network
- [ ] compute[01:02]: nova-compute, lvm storage


To update policy

kolla-ansible deploy -t $TAG

[https://docs.openstack.org/kolla-ansible/2023.2/admin/advanced-configuration.html#openstack-policy-customisation]

To update config of services

kolla-ansible reconfigure -t $TAG
